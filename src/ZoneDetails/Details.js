import React, { useState, useEffect } from 'react';
import { Error } from '../Error/Error';

export function Details(props) {
    const [selectedZone, setselectedZone] = useState({});
    const [timestamp, setTimeStamp] = useState(0);
    const [error, setError] = useState(false);
    const base_API_url = process.env.REACT_APP_API_BASE_URL;
    const { zone } = props;
    let dateString = "";

    useEffect(() => {
        fetch(`${base_API_url}/list-time-zone?key=XWSLLPX5RMIZ&format=json&zone=${zone}`)
            .then(response => response.json())
            .then(data => {
                const { zones } = data;
                setTimeStamp(zones[0].timestamp);
                setselectedZone(zones[0]);
            }).catch((error) => {
                setError(true);
                console.log("Error : ", error)
            });
    }, [zone, base_API_url]);

    const theDate = new Date(timestamp * 1000);
    dateString = theDate.toGMTString();
    setTimeout(function () {
        const newTimeStamp = timestamp + 5;
        setTimeStamp(newTimeStamp);
    }, 5000);

    return (
        <>
            {!error && <><div>Zone Details == == =</div>
                {Object.keys(selectedZone).length > 0 && (<><div><b>Country Name:</b> {selectedZone.countryName}</div>
                    <div><b>Zone Name:</b> {selectedZone.zoneName}</div>
                    <div><b>GMT Time:</b>{timestamp >= selectedZone.timestamp && <span>{dateString}</span>}</div></>)}</>}

            {error && <Error error={`API error in ${base_API_url}/list-time-zone?key=XWSLLPX5RMIZ&format=json&zone=${zone}`} />}
        </>
    );
}
