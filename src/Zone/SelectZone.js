import React, { useState, useEffect } from 'react';
import {Details} from '../ZoneDetails/Details';
import {Error} from '../Error/Error';

export function SelectZone() {
  const [listOfZone, setListOfZone] = useState([]);
  const [selectedZone, setSelectedZone] = useState("");
  const [error, setError] = useState(false);
  const base_API_url = process.env.REACT_APP_API_BASE_URL;

  /**
   * I am using "http://api.timezonedb.com/v2.1/list-time-zone?key=XWSLLPX5RMIZ&format=json" API link
   * as I am not getting data in given link "http://api.timezonedb.com/v2.1/list-time-zone?key=XWSLLPX5RMIZ&format=json&zone=Europ" (mention in document)
   */
  useEffect(() => {
    fetch(`${base_API_url}/list-time-zone?key=XWSLLPX5RMIZ&format=json`)
      .then(response => response.json())
      .then(data => {
        const { zones } = data;
        zones.sort((a, b) => (a.zoneName > b.zoneName ? 1 : -1));
        setListOfZone(zones);
      }).catch((error) => {
        setError(true);
        console.log("Error : ", error)
      });
  }, [base_API_url]);

  const handleChange = (event) => {
    setSelectedZone(event.target.value);
  };

  return (
    <>
      {!error && <><div>Select Zone </div>
      <select onChange={handleChange}>
        <option selected value="" disabled>Select Your Zone</option>
        {listOfZone.length && listOfZone.map(obj => <option key={obj.zoneName} value={obj.zoneName}>{obj.zoneName}</option>)}
      </select>

      {selectedZone && <Details zone={selectedZone}/>}</>}
      {error && <Error error={`API error in ${base_API_url}/list-time-zone?key=XWSLLPX5RMIZ&format=json`} />}
    </>
  );
}
